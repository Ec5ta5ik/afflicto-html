<?php

namespace Afflicto\HTML;
use Exception;
/**
 * @package afflicto/html
 * @version  1.1.0
 * @author Petter Thowsen <me@afflicto.net>
 */
class HTML {

	static protected $macros = array();

	static function macro($name, $closure) {
		self::$macros[$name] = $closure;
	}

	static function __callStatic($macro, $arguments = array()) {
		if (isset(self::$macros[$macro])) {
			$m = self::$macros[$macro];
			return call_user_func_array($m, $arguments);
		}
		trigger_error("Call to undefined method " .$macro);
	}

	static private function classComposerRecursive($class) {

	}

	static function classComposer($element) {
		$content = $element->getContent();
		if (is_array($content)) {
			foreach($content as $classes => $el) {
				if ($el instanceof IElement) {
					$el->addClass($classes);
					self::classComposer($el);
				}
			}
		}
		return $element;
	}
	
	static function youtube($url, $width = '560', $height = '315') {
		$matches = array();
		preg_match('/(?<=v=)[a-zA-Z0-9-_]+(?=&)|(?<=[0-9]\/)[^&\n]+|(?<=v=)[^&\n]+/', $url, $matches);
		$url = $matches[0];
		$el = new Element('iframe', true, null, array(
			'width' => $width,
			'height' => $height,
			'src' => '//www.youtube.com/embed/' .$url,
			'frameborder' => '0',
			'allowfullscreen' => 'allowfullscreen',
		));
		return $el;
	}

	
	static function vimeo($url, $width = '500', $height = '281') {
		$matches = array();
		preg_match('/[0-9]+/', $url, $matches);
		$url = $matches[0];
		$el = new Element('iframe', true, null, array(
			'width' => $width,
			'height' => $height,
			'src' => 'http://player.vimeo.com/video/' .$url .'?title=0&amp;byline=0&amp;portrait=0&amp;badge=0',
			'frameborder' => '0',
			'allowfullscreen' => 'allowfullscreen',
		));
		return $el;
	}


	static function html($content = array(), $attributes = array()) {
		$el = new Element('html', true, $content, $attributes);
		return $el;
	}


	static function head($content = array(), $attributes = array()) {
		$el = new Element('head', true, $content, $attributes);
		return $el;
	}


	static function title($content = array(), $attributes = array()) {
		$el = new Element('title', true, $content, $attributes);
		return $el;
	}


	static function base($href, $target = null) {
		$el = new Element('base', false);
		$el->attr('href', $href);
		if ($target) $el->attr('target', $target);
		return $el;
	}


	static function link($href, $rel = 'stylesheet', $type = 'text/css') {
		$el = new Element('link', false, null, array(
			'href' => $href,
			'stylesheet' => $stylesheet,
			'type' => $type,
		));
		return $el;
	}


	static function meta($name, $content) {
		$el = new Element('meta', false, null, array('name' => $name, 'content' => $content));
		return $el;
	}


	static function style($content, $type = 'text/css') {
		$el = new Element('style', true, $content, array('type' => $type));
		return $el;
	}


	static function script($content = array(), $attributes = array()) {
		$el = new Element('script', true, $content, $attributes);
		return $el;
	}


	static function noscript($content = array(), $attributes = array()) {
		$el = new Element('noscript', true, $content, $attributes);
		return $el;
	}


	static function body($content = array(), $attributes = array()) {
		$el = new Element('body', true, $content, $attributes);
		return $el;
	}


	static function section($content = array(), $attributes = array()) {
		$el = new Element('section', true, $content, $attributes);
		return $el;
	}


	static function nav($content = array(), $attributes = array()) {
		$el = new Element('nav', true, $content, $attributes);
		return $el;
	}


	static function article($content = array(), $attributes = array()) {
		$el = new Element('article', true, $content, $attributes);
		return $el;
	}


	static function aside($content = array(), $attributes = array()) {
		$el = new Element('aside', true, $content, $attributes);
		return $el;
	}


	static function h1($content = array(), $attributes = array()) {
		$el = new Element('h1', true, $content, $attributes);
		return $el;
	}


	static function h2($content = array(), $attributes = array()) {
		$el = new Element('h2', true, $content, $attributes);
		return $el;
	}


	static function h3($content = array(), $attributes = array()) {
		$el = new Element('h3', true, $content, $attributes);
		return $el;
	}


	static function h4($content = array(), $attributes = array()) {
		$el = new Element('h4', true, $content, $attributes);
		return $el;
	}


	static function h5($content = array(), $attributes = array()) {
		$el = new Element('h5', true, $content, $attributes);
		return $el;
	}


	static function h6($content = array(), $attributes = array()) {
		$el = new Element('h6', true, $content, $attributes);
		return $el;
	}


	static function header($content = array(), $attributes = array()) {
		$el = new Element('header', true, $content, $attributes);
		return $el;
	}


	static function footer($content = array(), $attributes = array()) {
		$el = new Element('footer', true, $content, $attributes);
		return $el;
	}


	static function address($content = array(), $attributes = array()) {
		$el = new Element('address', true, $content, $attributes);
		return $el;
	}


	static function main($content = array(), $attributes = array()) {
		$el = new Element('main', true, $content, $attributes);
		return $el;
	}


	static function p($content = array(), $attributes = array()) {
		$el = new Element('p', true, $content, $attributes);
		return $el;
	}


	static function hr($content = array(), $attributes = array()) {
		$el = new Element('hr', true, $content, $attributes);
		return $el;
	}


	static function pre($content = array(), $attributes = array()) {
		$el = new Element('pre', true, $content, $attributes);
		return $el;
	}


	static function blockquote($content = array(), $attributes = array()) {
		$el = new Element('blockquote', true, $content, $attributes);
		return $el;
	}


	static function ol($content = array(), $attributes = array()) {
		$el = new Element('ol', true, $content, $attributes);
		return $el;
	}


	static function ul($content = array(), $attributes = array()) {
		$el = new Element('ul', true, $content, $attributes);
		return $el;
	}


	static function li($content = array(), $attributes = array()) {
		$el = new Element('li', true, $content, $attributes);
		return $el;
	}


	static function dl($content = array(), $attributes = array()) {
		$el = new Element('dl', true, $content, $attributes);
		return $el;
	}


	static function dt($content = array(), $attributes = array()) {
		$el = new Element('dt', true, $content, $attributes);
		return $el;
	}


	static function dd($content = array(), $attributes = array()) {
		$el = new Element('dd', true, $content, $attributes);
		return $el;
	}


	static function figure($content = array(), $attributes = array()) {
		$el = new Element('figure', true, $content, $attributes);
		return $el;
	}


	static function figcaption($content = array(), $attributes = array()) {
		$el = new Element('figcaption', true, $content, $attributes);
		return $el;
	}


	static function div($content = array(), $attributes = array()) {
		$el = new Element('div', true, $content, $attributes);
		return $el;
	}


	static function a($href = '#', $content = array(), $attributes = array()) {
		$el = new Element('a', true, $content, $attributes);
		$el->attr('href', $href);
		return $el;
	}


	static function em($content = array(), $attributes = array()) {
		$el = new Element('em', true, $content, $attributes);
		return $el;
	}


	static function strong($content = array(), $attributes = array()) {
		$el = new Element('strong', true, $content, $attributes);
		return $el;
	}


	static function small($content = array(), $attributes = array()) {
		$el = new Element('small', true, $content, $attributes);
		return $el;
	}


	static function cite($content = array(), $attributes = array()) {
		$el = new Element('cite', true, $content, $attributes);
		return $el;
	}


	static function q($content = array(), $attributes = array()) {
		$el = new Element('q', true, $content, $attributes);
		return $el;
	}


	static function dfn($content = array(), $attributes = array()) {
		$el = new Element('dfn', true, $content, $attributes);
		return $el;
	}


	static function abbr($content = array(), $attributes = array()) {
		$el = new Element('abbr', true, $content, $attributes);
		return $el;
	}


	static function data($content = array(), $attributes = array()) {
		$el = new Element('data', true, $content, $attributes);
		return $el;
	}


	static function time($content = array(), $attributes = array()) {
		$el = new Element('time', true, $content, $attributes);
		return $el;
	}


	static function code($content = array(), $attributes = array()) {
		$el = new Element('code', true, $content, $attributes);
		return $el;
	}


	static function varr($content = array(), $attributes = array()) {
		$el = new Element('varr', true, $content, $attributes);
		return $el;
	}


	static function samp($content = array(), $attributes = array()) {
		$el = new Element('samp', true, $content, $attributes);
		return $el;
	}


	static function kbd($content = array(), $attributes = array()) {
		$el = new Element('kbd', true, $content, $attributes);
		return $el;
	}


	static function sub($content = array(), $attributes = array()) {
		$el = new Element('sub', true, $content, $attributes);
		return $el;
	}


	static function sup($content = array(), $attributes = array()) {
		$el = new Element('sup', true, $content, $attributes);
		return $el;
	}


	static function i($content = array(), $attributes = array()) {
		$el = new Element('i', true, $content, $attributes);
		return $el;
	}


	static function b($content = array(), $attributes = array()) {
		$el = new Element('b', true, $content, $attributes);
		return $el;
	}


	static function u($content = array(), $attributes = array()) {
		$el = new Element('u', true, $content, $attributes);
		return $el;
	}


	static function mark($content = array(), $attributes = array()) {
		$el = new Element('mark', true, $content, $attributes);
		return $el;
	}


	static function ruby($content = array(), $attributes = array()) {
		$el = new Element('ruby', true, $content, $attributes);
		return $el;
	}


	static function rt($content = array(), $attributes = array()) {
		$el = new Element('rt', true, $content, $attributes);
		return $el;
	}


	static function rp($content = array(), $attributes = array()) {
		$el = new Element('rp', true, $content, $attributes);
		return $el;
	}


	static function bdi($content = array(), $attributes = array()) {
		$el = new Element('bdi', true, $content, $attributes);
		return $el;
	}


	static function bdo($content = array(), $attributes = array()) {
		$el = new Element('bdo', true, $content, $attributes);
		return $el;
	}


	static function span($content = array(), $attributes = array()) {
		$el = new Element('span', true, $content, $attributes);
		return $el;
	}


	static function br($attributes = array()) {
		$el = new Element('br', false, null, $attributes);
		return $el;
	}


	static function wbr($content = array(), $attributes = array()) {
		$el = new Element('wbr', true, $content, $attributes);
		return $el;
	}


	static function ins($content = array(), $attributes = array()) {
		$el = new Element('ins', true, $content, $attributes);
		return $el;
	}


	static function del($content = array(), $attributes = array()) {
		$el = new Element('del', true, $content, $attributes);
		return $el;
	}


	static function img($src, $attributes = array()) {
		$el = new Element('img', false, null, $attributes);
		$el->attr('src', $src);
		return $el;
	}


	static function iframe($content = array(), $attributes = array()) {
		$el = new Element('iframe', true, $content, $attributes);
		return $el;
	}


	static function embed($content = array(), $attributes = array()) {
		$el = new Element('embed', true, $content, $attributes);
		return $el;
	}


	static function object($content = array(), $attributes = array()) {
		$el = new Element('object', true, $content, $attributes);
		return $el;
	}


	static function param($content = array(), $attributes = array()) {
		$el = new Element('param', true, $content, $attributes);
		return $el;
	}


	static function video($content = array(), $attributes = array()) {
		$el = new Element('video', true, $content, $attributes);
		return $el;
	}


	static function audio($content = array(), $attributes = array()) {
		$el = new Element('audio', true, $content, $attributes);
		return $el;
	}


	static function source($content = array(), $attributes = array()) {
		$el = new Element('source', true, $content, $attributes);
		return $el;
	}


	static function track($content = array(), $attributes = array()) {
		$el = new Element('track', true, $content, $attributes);
		return $el;
	}


	static function canvas($content = array(), $attributes = array()) {
		$el = new Element('canvas', true, $content, $attributes);
		return $el;
	}


	static function map($content = array(), $attributes = array()) {
		$el = new Element('map', true, $content, $attributes);
		return $el;
	}


	static function area($content = array(), $attributes = array()) {
		$el = new Element('area', true, $content, $attributes);
		return $el;
	}


	static function svg($content = array(), $attributes = array()) {
		$el = new Element('svg', true, $content, $attributes);
		return $el;
	}


	static function math($content = array(), $attributes = array()) {
		$el = new Element('math', true, $content, $attributes);
		return $el;
	}


	static function table($content = array(), $attributes = array()) {
		$el = new Element('table', true, $content, $attributes);
		return $el;
	}


	static function caption($content = array(), $attributes = array()) {
		$el = new Element('caption', true, $content, $attributes);
		return $el;
	}


	static function colgroup($content = array(), $attributes = array()) {
		$el = new Element('colgroup', true, $content, $attributes);
		return $el;
	}


	static function col($content = array(), $attributes = array()) {
		$el = new Element('col', true, $content, $attributes);
		return $el;
	}


	static function tbody($content = array(), $attributes = array()) {
		$el = new Element('tbody', true, $content, $attributes);
		return $el;
	}


	static function thead($content = array(), $attributes = array()) {
		$el = new Element('thead', true, $content, $attributes);
		return $el;
	}


	static function tfoot($content = array(), $attributes = array()) {
		$el = new Element('tfoot', true, $content, $attributes);
		return $el;
	}


	static function tr($content = array(), $attributes = array()) {
		$el = new Element('tr', true, $content, $attributes);
		return $el;
	}


	static function td($content = array(), $attributes = array()) {
		$el = new Element('td', true, $content, $attributes);
		return $el;
	}


	static function th($content = array(), $attributes = array()) {
		$el = new Element('th', true, $content, $attributes);
		return $el;
	}


	static function form($content = array(), $attributes = array()) {
		$el = new Element('form', true, $content, $attributes);
		return $el;
	}


	static function fieldset($content = array(), $attributes = array()) {
		$el = new Element('fieldset', true, $content, $attributes);
		return $el;
	}


	static function legend($content = array(), $attributes = array()) {
		$el = new Element('legend', true, $content, $attributes);
		return $el;
	}


	static function label($content = array(), $attributes = array()) {
		$el = new Element('label', true, $content, $attributes);
		return $el;
	}


	static function input($content = array(), $attributes = array()) {
		$el = new Element('input', true, $content, $attributes);
		return $el;
	}


	static function button($content = array(), $attributes = array()) {
		$el = new Element('button', true, $content, $attributes);
		return $el;
	}


	static function select($content = array(), $attributes = array()) {
		$el = new Element('select', true, $content, $attributes);
		return $el;
	}


	static function datalist($content = array(), $attributes = array()) {
		$el = new Element('datalist', true, $content, $attributes);
		return $el;
	}


	static function optgroup($content = array(), $attributes = array()) {
		$el = new Element('optgroup', true, $content, $attributes);
		return $el;
	}


	static function option($content = array(), $attributes = array()) {
		$el = new Element('option', true, $content, $attributes);
		return $el;
	}


	static function textarea($content = array(), $attributes = array()) {
		$el = new Element('textarea', true, $content, $attributes);
		return $el;
	}


	static function keygen($content = array(), $attributes = array()) {
		$el = new Element('keygen', true, $content, $attributes);
		return $el;
	}


	static function output($content = array(), $attributes = array()) {
		$el = new Element('output', true, $content, $attributes);
		return $el;
	}


	static function progress($content = array(), $attributes = array()) {
		$el = new Element('progress', true, $content, $attributes);
		return $el;
	}


	static function meter($content = array(), $attributes = array()) {
		$el = new Element('meter', true, $content, $attributes);
		return $el;
	}


	static function details($content = array(), $attributes = array()) {
		$el = new Element('details', true, $content, $attributes);
		return $el;
	}


	static function summary($content = array(), $attributes = array()) {
		$el = new Element('summary', true, $content, $attributes);
		return $el;
	}

	static function menu($content = array(), $attributes = array()) {
		$el = new Element('menu', true, $content, $attributes);
		return $el;
	}

	static function menuitem($content = array(), $attributes = array()) {
		$el = new Element('menuitem', true, $content, $attributes);
		return $el;
	}

}