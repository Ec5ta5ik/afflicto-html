<?php
/*-------------------------------------------------
|	First, we require the HTML.php static class.
|   It will load the IElement interface and
|	the Element class.
|------------------------------------------------*/
require_once 'vendor/autoload.php';



/*-------------------------------------------------
|	Let's alias the HTML class to save us from
|   Typing the entire namespace.
|------------------------------------------------*/
use Afflicto\HTML\HTML;



//here's how to create an "image link".
echo HTML::a('http://google.com', HTML::img('http://lorempixel.com/200/200/nature'));


//html
$html = HTML::html("HTML HERE");

echo $html;


/*-------------------------------------------------
|	set,get, add/remove content
|------------------------------------------------*/
$div = HTML::div();
$div->header = HTML::header('header');
$div->section = HTML::section('section');
$div->footer = HTML::footer('footer');

echo $div;//header section footer

//let's remove the footer
unset($div->footer);

echo $div;


/*-------------------------------------------------
|	Create your own "macros".
|------------------------------------------------*/
HTML::macro('icon', function($icon) {
	$i = HTML::i();
	$i->addClass('icon-' .$icon);//like twitter bootsrap's glyphicons.
	return $i;
});

//...and use them
echo HTML::icon('user');



/*-------------------------------------------------
|	Youtube & Vimeo videos
|------------------------------------------------*/
//create a youtube video...
echo HTML::youtube('http://www.youtube.com/watch?v=kkGeOWYOFoA');

//...or a vimeo video
echo HTML::vimeo('http://vimeo.com/46141955');



/*-------------------------------------------------
|	Create complex HTML structures/layouts.
|------------------------------------------------*/
$layout = HTML::div(
	array(
		HTML::div(array(HTML::h1('Hello!'), HTML::p('Lorem ipsum dolor sit amet')), array('class' => 'row')),
		HTML::div(array(HTML::h2('hello again', HTML::p('Lorem ipsun dolor sit amet, again...'))), array('class' => 'row')),
	), 
	array('class' => 'container')
);
echo $layout;


/*-------------------------------------------------
|	Use the classComposer for an array where the
|	array keys become the class[es] of their values.
|	It's magic!
|------------------------------------------------*/
$layout2 = HTML::classComposer(html::div(array(
	'key1' => HTML::div('this div has a class of "key1"'),
	'key2' => HTML::div(array('hello', 'test' => html::div('this div has a class of "test"')))
	))
);

echo $layout2;