<?php

namespace Afflicto\HTML;

class Element implements IElement {
	
	protected $tagName = 'div';
	protected $endTag = true;
	protected $content = null;
	protected $attributes = array();
	protected $classes = array();
	
	public function __construct($tagName = 'div', $endTag = false, $content = null, $attributes = array()) {
		$this->tagName = $tagName;
		$this->endTag = $endTag;
		$this->content = $content;
		$this->attributes = (is_array($attributes)) ? $attributes : array();
		if (isset($this->attributes['class'])) {
			$this->classes = explode(' ', $this->attributes['class']);
			unset($this->attributes['class']);
		}
	}
	
	public function with($mixed) {
		$this->content = $mixed;
		return $this;
	}

	public function clear() {
		$this->content = array();
	}

	public function getContent() {
		return $this->content;
	}

	public function __set($key, $value) {
		if (!is_array($this->content)) $this->content = array($this->content);
		$this->content[$key] = $value;
	}

	public function __get($key) {
		return (isset($this->content[$key])) ? $this->content[$key] : null;
	}

	public function __unset($key) {
		if (isset($this->content[$key])) {
			unset($this->content[$key]);
		}
		return false;
	}

	public function __isset($key) {
		return isset($this->content[$key]);
	}

	public function attr($key, $value = null) {
		if (!is_string($key)) throw new Exception("attr expects parameter 1 to be of type string", 1);
		if ($value == null) return (isset($this->attributes[$key])) ? $this->attributes[$key] : null;
		if ($key == 'class') {
			$this->addClass($value);
		}else {
			$this->attributes[$key] = $value;
			return $this;
		}
	}

	public function addClass($classes) {
		$classes = explode(' ', $classes);
		foreach($classes as $c) {
			if (!in_array($c, $this->classes)) $this->classes[] = $c;
		}
		return $this;
	}

	public function removeClass($classes) {
		foreach(explode(' ', $classes) as $c) {
			if (in_array($c, $this->classes)) unset($this->classes[array_search($c, $this->classes)]);
		}
		return $this;
	}

	public function displayAttributes() {
		$str = '';
		//merge classes in 'classes' with 'attributes'
		$attributes = $this->attributes;
		$attributes['class'] = implode(' ', $this->classes);
		foreach($attributes as $name => $value) {
			$str .= ' ' .$name .'="' .str_replace('"', '\'', $value) .'"';
		}
		return $str;
	}

	public function displayContent($c = null) {
		$c = ($c == null) ? $this->content : $c;
		$str = '';
		if (is_string($c)) {
			$str .= $c;
		}else if (is_array($c)) {
			foreach($c as $value) {
				$str .= $this->displayContent($value);
			}
		}else if (is_object($c)) {
			if ($c instanceof IElement) {
				$str .= $c->display();
			}
		}
		return $str;
	}

	public function display() {
		$str = '<' .$this->tagName .$this->displayAttributes() .'>';

		if ($this->endTag) {
			$str .= $this->displayContent() .'</' .$this->tagName .'>';
		}

		return $str;
	}

	public function __toString() {
		return $this->display();
	}

}