<?php

namespace Afflicto\HTML;

interface IElement {
	
	function displayAttributes();

	function displayContent();

	public function display();

	public function __toString();
	
}